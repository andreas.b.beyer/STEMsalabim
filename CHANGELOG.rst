What's new
==========

STEMsalabim 5.0.0
-----------------

February 28th, 2019

**IMPORTANT**

The parameters `application.verbose` and `simulation.skip_simulation` are deprecated now.
The groups `adf/adf_intensities`, `cbed/cbed_intensities`, and `adf/center_of_mass` now have
a dimension for energy loss. It is usually `1` unless plasmon scattering feature is used.

Highlights
^^^^^^^^^^

- Speed improvements by increasing the grid sizes to match efficient FFT sizes. Note, that this may result
  in a higher simulation grid density than specified in `grating.density` parameter!
- Alternative parallelization scheme, see :ref:`parallelization-scheme`. When appropriate, different MPI procs
  now calculate different frozen phonon configurations / defoci in parallel. This reduces the required amount
  of communication between the processors.
- Automatic calculation of `center of mass` of the CBEDs for all ADF points. The COMs are calculated when
  `adf.enabled = true` and stored in the NC file next to `adf/adf_intensities` in `adf/center_of_mass`. Unit is mrad.
- New executables `ssb-mkin` and `ssb-run`. The former prepares an **input** NC file from which the latter can run
  the simulation. This has multiple advantages. See :ref:`simulation-structure` for more information.
- Single plasmon scattering.


Other changes
^^^^^^^^^^^^^

- Removed `application.verbose` parameter.
- Removed `simulation.skip_simulation`.
- Ability to disable thermal displacements via `frozen_phonon.enable = false` parameter.
- Fixed a serious bug with the integrated defocus averaging.
- Input XYZ files can now contain more than one space or TAB character for column separation.
- Removed Doxygen documentation and doc string comments.
- Default FFTW planning is now `FFTW_MEASURE`. This improves startup times of the simulation slightly.
- Changed the chunking of the `adf/adf_intensities` and `cbed/cbed_intensities` variables for faster write speed.
- Added `AMBER/slice_coordinates` variable to the output file, that contains the `z` coordinate of the upper boundary
  of each slice in nm.
- Removed HTTP reporting and CURL dependency.
- Significant code refactoring and some minor bugs fixed.
- Improved documentation.


STEMsalabim 4.0.1, 4.0.2
------------------------

March 23rd, 2018
March 21st, 2018

- Bug fixes
- Changed chunking of the ADF variable


STEMsalabim 4.0
---------------

March 9th, 2018

**IMPORTANT**

I'm releasing this version as 4.0.0, but neither the input nor output files changed. The parameter `precision` has
become deprecated and there is a parameter `tmp-dir`. Please see the documentation.

- Removed option for double precision. When requested, this may be re-introduced, but it slowed down compilation times
  and made the code significantly more complicated. The multislice algorithm with all its approximations, including the
  scattering factor parametrization, is not precise enough to make the difference between single and double precision
  significant.
- Improved the Wave class, so that some important parts can now be vectorized by the compiler.
- Introduced some more caches, so that performance could greatly be improved. STEMsalabim should now be about twice as
  fast as before.
- Results of the MPI processors are now written to temporary files and merged after each configuration is finished.
  This removes many MPI calls which tended to slow down the simulation. See the `--tmp-dir` parameter.
- Moved the `Element`, `Atom`, and `Scattering` classes to their own (isolated) library `libatomic`. This is easier to
  maintain.
- Simplified MPI communication by getting rid of serialization of C++ objects into char arrays. This is too error-prone
  anyway.
- Added compatibility with the Intel parallel studio (Compilers, MKL for FFTs, Intel MPI).
  Tested with Intel 17 only.
- Some minor fixes and improvements.


STEMsalabim 3.1.0, 3.1.1, 3.1.2, 3.1.3, 3.1.4
---------------------------------------------

February 23nd, 2018

- Added GPL-3 License
- Moved all the code to Gitlab
- Moved documentation to readthedocs.org
- Added Gitlab CI

STEMsalabim 3.0.1 and 3.0.2
---------------------------

February 22nd, 2018

- Fixed a few bugs
- Improved the CMake files for better build process

STEMsalabim 3.0.0
-----------------

January 3rd, 2018

- Reworked input/output file format.
- Reworked CBED storing. Blank areas due to bandwidth limiting are now removed.
- Changes to the configuration, mainly to defocus series.
- Compression can be switched on and off via config file now.
- Prepared the project for adding a Python API in the future.
- Added tapering to smoothen the atomic potential at the edges as explained in
  `I. Lobato, et al, Ultramicroscopy 168, 17 (2016) <https://www.sciencedirect.com/science/article/pii/S030439911630081X>`_.
- Added analysis scripts for Python and MATLAB to the Si 001 example.

STEMsalabim 2.0.0
-----------------

August 1st, 2017

- Changed Documentation generator to Sphinx
- Introduced a lot of memory management to prevent memory fragmentation bugs
- split STEMsalabim into a core library and binaries to ease creation of tools
- Added diagnostics output with --print-diagnostics
- Code cleanup and commenting


STEMsalabim 2.0.0-beta2
-----------------------

April 20th, 2017

-  Added possibility to also save CBEDs, i.e., the kx/ky resolved
   intensities in reciprocal space.
-  Improved documentation.
-  Switched to NetCDF C API. Dependency on NetCDF C++ is dropped.
-  Switched to distributed (parallel) writing of the NC files, which is
   required for the CBED feature. This requires NetCDF C and HDF5 to be
   compiled with MPI support.

STEMsalabim 2.0.0-beta
----------------------

March 27th, 2017

-  Lots of code refactoring and cleanup
-  Added Doxygen doc strings
-  Added Markdown documentation and ``make doc`` target to build this
   website.
-  Refined the output file structure
-  Added HTTP reporting feature
-  Added ``fixed_slicing`` option to fix each atom's slice througout the
   simulation
-  Got rid of the boost libraries to ease compilation and installation

STEMsalabim 1.0
---------------

November 18th, 2016

-  Initial release.
