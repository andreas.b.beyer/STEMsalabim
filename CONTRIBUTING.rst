How to contribute
=================

We welcome any contributions to STEMsalabim, be it code, issues, feature requests, or feedback!

General Notes
-------------

Development of *STEMsalabim*  takes place on `Gitlab.com <https://gitlab.com/STRL/STEMsalabim>`_.
Please use Gitlab's features to create issues or merge requests.

Coding style
------------

Please try to adhere to the coding conventions you find in the source code of *STEMsalabim*. If you use an IDE
from `Jetbrains <https://www.jetbrains.com/idea/>`_, such as CLion (which we warmly recommend), you can import the
Code Style settings from :code:`misc/Jetbrains-Coding-Style.xml`.

