File formats
============

A STEMsalabim simulation is set-up via **input files** and its results are stored in an **output file**. The file for
configuring a simulation is described in detail at :ref:`parameter-file`. Here, we describe the format of the **crystal
file**, i.e., the atomic information about the specimen, and the **output file**, in which the results are stored.

.. _crystal-file:

Crystal file format
-------------------

The crystal file is expected to be in `XYZ format <https://en.wikipedia.org/wiki/XYZ_file_format>`_.

1. The **first line** contains the number of atoms.

2. The **second line** contains the cell dimension in x,y,z direction as floating point numbers in units of ``nm``,
   separated by a space. Optionally, it can contain the x, y, z dimensions in the .exyz format: ::

      Lattice="lx 0.0 0.0 0.0 ly 0.0 0.0 0.0 lz"

3. The atomic information is given from the **third line onwards**, with each line corresponding to a single atom. Each
   line must have *exactly 5 columns*:

    - The atomic species as elemental abbreviation (e.g., ``Ga`` for gallium)
    - the x,y,z coordinates as floating point numbers in units of ``nm``
    - the mean square displacement for the frozen lattice dislocations in units of :math:`nm^2`.
    - (**optional**) The id of the slice this atom belongs to. This can be used to do custom slicing.

Below is a very brief, artificial example (without custom slicing): ::

    5
    1.0 2.0 10.0
    Ga  0.0  0.0   0.0   1e-5
    P   0.2  0.1   0.0   2e-5
    Ga  0.0  0.0   1.0   1e-5
    P   1.2  0.1   0.0   2e-5
    O   1.0  2.0  10.0   0.0


.. note:: Atomic coordinates outside of the cell are periodically wrapped in ``x`` and ``y`` and clipped to the
   simulation box in ``z`` direction!

.. _output-file:

Output file format
------------------

All results of a STEMsalabim simulation are written to a binary `NetCDF <https://en.wikipedia.org/wiki/NetCDF>`_ file.
The NetCDF format is based on the `Hierarchical Data Format <https://en.wikipedia.org/wiki/Hierarchical_Data_Format>`_
and there are libraries to read the data for many programming languages.

The structure of NetCDF files can be inspected using the handy tool ``ncdump -h YOUR_FILE.nc`` (don't forget the ``-h``
parameter, otherwise the whole content of the file is dumped!). Here is the output of an example run: ::


    netcdf out {

    group: AMBER {
      dimensions:
        atom = 164140 ;
        elements = 1 ;
        spatial = 3 ;
        cell_spatial = 3 ;
        cell_angular = 3 ;
        label = 6 ;
        frame = 10 ;
        slices = 142 ;
        grid_x = 490 ;
        grid_y = 490 ;
      variables:
        char spatial(spatial) ;
        char cell_spatial(cell_spatial) ;
        char cell_angular(cell_angular, label) ;
        float coordinates(frame, atom, spatial) ;
            coordinates:unit = "nanometer" ;
        float lattice_coordinates(frame, atom, spatial) ;
        float cell_lengths(frame, cell_spatial) ;
            cell_lengths:unit = "nanometer" ;
        float cell_angles(frame, cell_angular) ;
            cell_angles:unit = "degree" ;
        float radius(frame, atom) ;
            radius:unit = "nanometer" ;
        float msd(frame, atom) ;
        int slice(frame, atom) ;
        float slice_coordinates(slices) ;
        short element(frame, atom) ;
        float system_lengths(cell_spatial) ;
        float system_angles(cell_spatial) ;
        char atom_types(elements, label) ;

      // group attributes:
            :Conventions = "AMBER" ;
            :ConventionVersion = "1.0" ;
            :program = "STEMsalabim" ;
            :programVersion = "5.0.0b" ;
            :title = "sim" ;
      } // group AMBER

    group: runtime {

      // group attributes:
            :programVersionMajor = "5" ;
            :programVersionMinor = "0" ;
            :programVersionPatch = "0b" ;
            :gitCommit = "f1dcc606c9a78b12fc3afda9496f638992b591bf" ;
            :title = "sim" ;
            :UUID = "8dce768e-f1d6-4876-bb20-c301e3e323f8" ;
            :time_start = "2019-02-12 13:25:43" ;
            :time_stop = "2019-02-13 00:06:05" ;
      } // group runtime

    group: params {
      dimensions:
        defocus = 1 ;
        plasmon_energies = 51 ;
      variables:
        float defocus(defocus) ;
        float defocus_weights(defocus) ;
        float plasmon_energies(plasmon_energies) ;

      // group attributes:
            :program_arguments = "--params=inp.cfg --num-threads=4 --tmp-dir=/local --output-file=out.nc" ;
            :config_file_contents = "..." ;

      group: application {

        // group attributes:
                :random_seed = 967613772U ;
        } // group application

      group: simulation {

        // group attributes:
                :title = "sim" ;
                :normalize_always = 0US ;
                :bandwidth_limiting = 1US ;
                :output_file = "out.nc" ;
                :output_compress = 0US ;
        } // group simulation

      group: probe {

        // group attributes:
                :c5 = 5000000. ;
                :cs = 2000. ;
                :astigmatism_ca = 0. ;
                :defocus = -0. ;
                :fwhm_defoci = 6. ;
                :num_defoci = 1U ;
                :astigmatism_angle = 0. ;
                :min_apert = 0. ;
                :max_apert = 15.07 ;
                :beam_energy = 200. ;
                :scan_density = 40. ;
        } // group probe

      group: specimen {

        // group attributes:
                :max_potential_radius = 0.3 ;
                :crystal_file = "Si_110_10x10x200_300K.xyz" ;
        } // group specimen

      group: grating {

        // group attributes:
                :density = 90. ;
                :nx = 490U ;
                :ny = 490U ;
                :slice_thickness = 0.76806 ;
        } // group grating

      group: adf {

        // group attributes:
                :enabled = 1US ;
                :x = 0.5, 0.6 ;
                :y = 0.5, 0.6 ;
                :detector_min_angle = 0. ;
                :detector_max_angle = 150. ;
                :detector_num_angles = 151U ;
                :detector_interval_exponent = 1.f ;
                :average_configurations = 1US ;
                :average_defoci = 1US ;
                :save_slices_every = 10U ;
        } // group adf

      group: cbed {

        // group attributes:
                :enabled = 1US ;
                :x = 0.5, 0.6 ;
                :y = 0.5, 0.6 ;
                :size = 0U, 0U ;
                :average_configurations = 1US ;
                :average_defoci = 0US ;
                :save_slices_every = 101U ;
        } // group cbed

      group: frozen_phonon {

        // group attributes:
                :number_configurations = 10U ;
                :fixed_slicing = 1US ;
                :enabled = 1US ;
        } // group frozen_phonon

      group: plasmon_scattering {

        // group attributes:
                :enabled = 1US ;
                :simple_mode = 0US ;
                :plural_scattering = 0US ;
                :max_energy = 25.f ;
                :energy_grid_density = 2.f ;
                :mean_free_path = 128.f ;
                :plasmon_energy = 16.9f ;
                :plasmon_fwhm = 4.f ;
        } // group plasmon_scattering
      } // group params

    group: adf {
      dimensions:
        adf_position_x = 22 ;
        adf_position_y = 22 ;
        adf_detector_angle = 151 ;
        adf_defocus = 1 ;
        adf_phonon = 1 ;
        adf_slice = 15 ;
        coordinate_dim = 2 ;
        adf_plasmon_energies = 51 ;
      variables:
        float adf_intensities(adf_defocus, adf_position_x, adf_position_y, adf_phonon, adf_slice, adf_plasmon_energies, adf_detector_angle) ;
        float center_of_mass(adf_defocus, adf_position_x, adf_position_y, adf_phonon, adf_slice, adf_plasmon_energies, coordinate_dim) ;
        double adf_probe_x_grid(adf_position_x) ;
        double adf_probe_y_grid(adf_position_y) ;
        double adf_detector_grid(adf_detector_angle) ;
        double adf_slice_coords(adf_slice) ;
      } // group adf

    group: cbed {
      dimensions:
        cbed_position_x = 22 ;
        cbed_position_y = 22 ;
        cbed_k_x = 327 ;
        cbed_k_y = 327 ;
        cbed_defocus = 1 ;
        cbed_phonon = 1 ;
        cbed_slice = 2 ;
        cbed_plasmon_energies = 51 ;
      variables:
        float cbed_intensities(cbed_defocus, cbed_position_x, cbed_position_y, cbed_phonon, cbed_slice, cbed_plasmon_energies, cbed_k_x, cbed_k_y) ;
        double cbed_probe_x_grid(cbed_position_x) ;
        double cbed_probe_y_grid(cbed_position_y) ;
        double cbed_x_grid(cbed_k_x) ;
        double cbed_y_grid(cbed_k_y) ;
        double cbed_slice_coords(cbed_slice) ;
      } // group cbed
    }

The structure of NetCDF files is hierarchical and organized in groups. The following groups are written by
STEMsalabim:

AMBER
~~~~~

This group contains the atomic coordinates, species, displacements, radii, etc. for the complete crystal for each single
calculated frozen lattice configuration, as well as for each calculated defocus value. The AMBER group content is
compatible with the `AMBER specifications <http://ambermd.org/netcdf/nctraj.xhtml>`_. A STEMsalabim NetCDF file can
be opened seamlessly with the `Ovito <http://www.ovito.org/>`_ crystal viewer.

.. csv-table::
   :file: table_nc_amber.csv


runtime
~~~~~~~

.. csv-table::
   :file: table_nc_runtime.csv


params
~~~~~~

.. note::  The ``params`` group contains subgroups with attributes that correspond exactly to the simulation
    parameters as written, except

     - ``/params/application/random_seed`` is set to the generated random seed
     - ``/params/grating/nx`` and ``/params/grating/ny`` contain the simulation grid size used.

.. csv-table::
   :file: table_nc_params.csv

adf
~~~

.. csv-table::
   :file: table_nc_adf.csv

cbed
~~~~

.. csv-table::
   :file: table_nc_cbed.csv

Reading NC Files
----------------

For a detailed view of the structure, we suggest using the `ncdump
<http://www.unidata.ucar.edu/software/netcdf//old_docs/docs_4_1/netcdf/ncdump.html>`_ utility: ``ncdump -h
some_results_file.nc``. As the underlying file format of NetCDF is HDF5, you may use any other HDF5 viewer to have a
look at the results.

There are NetCDF bindings for most popular programming languages.

1. In MATLAB, we recommend using ``h5read()`` and the `other HDF5 functions
   <https://de.mathworks.com/help/matlab/high-level-functions.html>`_.
2. For Python, use the
   `netCDF4 <http://unidata.github.io/netcdf4-python/>`_ module.
