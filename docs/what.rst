.. _what:

What STEMsalabim calculates
===========================

Background
----------

STEMsalabim simulates the image of a
`transmission electron microscope (TEM) <https://en.wikipedia.org/wiki/Transmission_electron_microscopy>`_
in scanning mode, i.e., with a convergent electron beam scanning over the sample. (TEM can also be operated
using plane wave illumination, but this is (currently) not supported by STEMsalabim.

In scanning TEM (STEM) a narrowly focused beam of high energy electrons is focused onto a very thin layer of some
material. Passing through the sample, the incident electrons are scattered by the sample's
coulomb potential, which is composed of the atomic cores and the sample's electrons. After leaving the sample the amount
and direction of scattering is detected to gain insight into the sample's atomic structure, chemical composition,
electric fields, thickness, etc.

Instead of repeating the details of STEM here, we refer the reader to some good literature on the topic:

 - `Transmission Electron Microscopy <https://www.springer.com/de/book/9780387765006>`_ by David B. Williams and Barry C. Carter
 - `Advanced computing in electron microscopy <http://dx.doi.org/10.1007/978-1-4419-6533-2>`_ by Earl J. Kirkland

For the details on STEMsalabim's algorithm and implementation, the following paper is instructive:

 - `STEMsalabim: A high-performance computing cluster friendly code for scanning transmission electron microscopy image simulations of thin specimens <http://dx.doi.org/10.1016/j.ultramic.2017.03.010>`_

Probe wave function
-------------------

The electron probe in the STEM must be modelled as a coherent electronic wave, since diffraction plays an essential
role in STEM. In STEM simulations, the focused probe wave function :math:`\Psi_0(x,y)` is modelled as a complex 2D
object, and its propagation in beam direction (here, :math:`z`) is simulated.

The probe wave function encapsulates all characteristics of the microscope, such as aberrations, aperture, etc. It is
calculated via

.. math::
    \Psi^{x_p, y_p}_0(x, y) = B \mathcal{F}^{-1}\left(A(k_x, k_y)\exp\left[-i\chi(k_x, k_y) + 2\pi i (k_x x_p + k_y y_p)\right]\right)\, ,

with a normalization constant :math:`B`, the aperture function :math:`A(k_x, k_y)`, and the aberration phase
error :math:`\chi(k_x, k_y)`:

.. math::
    \chi(k_x, k_y) = \frac{\pi \alpha^2}{\lambda} \left(-\Delta f + C_a \cos(2\phi - 2\phi_a)+\frac{1}{2}C_s\alpha^2 + \frac{1}{3} C_5\alpha^4\right)\, .

The aberration coefficients :math:`C_a, C_s, C_5, \Delta f, \phi_a` and the aperture radius entering :math:`A(k_x, k_y)`
are parameters of the microscope and can be freely defined in STEMsalabim. (See :ref:`parameter-file`)

Multislice Simulation
---------------------

While passing through the thin sample,
the wave function is scattered by the sample's coulomb potential. In STEMsalabim, the multi-slice algorithm is implemented
to model the interaction of the incoming probe wave function with the sample's coulomb potential.

In the multi-slice approximation, the sample is divided into thin slices perpendicular to the beam direction. When
the electrons of the probe have much higher energy than those of the specimen, and the slices are thin enough, the
interaction between an incoming wave function and one of these slices can be described via

.. math::
    \Psi_f(x,y) = t(x,y)\Psi_i(x,y)

where :math:`\Psi_i(x,y)` is the incoming and :math:`\Psi_f(x,y)` the outgoing WF. The 2D *transmission function* of the
slice is :math:`t(x,y)` depends on the *projected coulomb potential* :math:`v(x,y)` of the slice:

.. math::
    t(x,y) = \exp(-i\sigma v(x,y)

with :math:`\sigma` being some relativistic interaction parameter. Clearly, the transmission function :math:`t(x,y)`
merely modifies the phase of the wave function and is therefore sometimes called a *weak phase object*.

To model the interaction of the probe WF :math:`\Psi_0(x,y)` with every slice, it is subsequently multiplied with the
transmission function of each single slice. In between the slices, the WF is propagated to the next slice by multiplication
 with a Fresnel propagator

.. math::
    p(k_x, k_y) = \exp(i\pi (k_x^2 +k_y^2) \lambda dz)

with wavelength :math:`\lambda` and slice thickness :math:`dz`. Since the propagator is defined in :math:`k`-space,
an iteration of the multi-slice approximation is calculated as

.. math::
    \Psi_{n+1}(x,y) = \mathscr{F}^{-1}\left[p(k_x, k_y)\mathscr{F}\left[t(x,y)\Psi_n(x,y)\right]\right]

where :math:`\mathscr{F}` and :math:`\mathscr{F}^{-1}` are forward and backward 2D Fourier transformations.

Detector
--------

After the multi-slice simulation, the absolute intensity of the diffracted WF (in :math:`k`-space) :math:`|\Psi(k_x, k_y)|^2`
is detected. STEMsalabim supports writing the 2D diffractogram directly to the output file, but it can also calculate
and write the angular dependency :math:`|\Psi(|k|)|^2`. (See :ref:`parameter-file` and :ref:`output-file` for more information.)