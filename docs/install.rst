.. _installing:

Installing STEMsalabim
======================

Requirements
------------

The following libraries and tools are needed to successfully compile the code:

* A C++11 compiler (such as `gcc/g++ <https://gcc.gnu.org/>`_ or `intel compiler suite <https://software.intel.com/en-us/compilers>`_).
* `CMake <https://cmake.org/>`_ > 3.3
* `NetCDF <https://www.unidata.ucar.edu/software/netcdf/>`_
* `libConfig <http://www.hyperrealm.com/libconfig/>`_ >= 1.5
* `FFTW3 <http://www.fftw.org/>`_ or `Intel's MKL <https://software.intel.com/en-us/mkl>`_
* An MPI implementation (such as `OpenMPI <http://www.open-mpi.de/>`_)

.. note:: You may find some of the requirements in the repositories of your Linux distribution, at least the compiler,
          CMake, and OpenMPI. On Debian or Ubuntu Linux, for example, you can simply run the following command
          to download and install all the requirements:
          ::

              $ apt-get install build-essential      \
                                cmake                \
                                libconfig++-dev      \
                                libfftw3-dev         \
                                libnetcdf-dev        \
                                libopenmpi-dev       \
                                openmpi-bin


.. Tip:: Most of the computing time is spent calculating Fourier transforms, so it is beneficial for STEMsalabim
         to use optimized FFT libraries. Sometimes, compiling FFTW or MKL on the target machine enables
         optimizations that are not available in precompiled binaries, so this may be worth a try.


Downloading the source code
---------------------------

We recommend you download the latest stable release (|release|) from the
`Releases page <https://gitlab.com/STRL/STEMsalabim/tags>`_. If you want the latest features and/or bugfixes,
you can also clone the repository using

::

    $ git clone https://gitlab.com/STRL/STEMsalabim.git
    $ git checkout devel # only if you want the devel code.


Building STEMsalabim
--------------------

Extract the code archive to some folder on your hard drive, e.g.

::

    $ cd /tmp
    $ tar xzf stemsalabim-VERSION.tar.gz

Then, create a build directory and run CMake to generate the build configuration:

::

    $ mkdir /tmp/stemsalabim-build
    $ cd /tmp/stemsalabim-build
    $ cmake ../stemsalabim-VERSION

Please refer to the `CMake documentation <https://cmake.org/documentation/>`_ for instructions how to specify library
paths and other environment variables, in case the above commands failed. When your libraries exist at non-standard places
in your file system, you can specify the search paths as follows:

::

    $ cmake ../stemsalabim-VERSION                                            \
        -DFFTW_ROOT=/my/custom/fftw/                                          \
        -DLIBCONFIG_ROOT=/my/custom/libconfig/                                \
        -DNETCDF_INCLUDE_DIR=/my/custom/netcdf/include                        \
        -DCMAKE_INSTALL_PREFIX=/usr/local                                     \
        -DCMAKE_EXE_LINKER_FLAGS='-Wl,-rpath,/my/custom/lib64:/my/custom/lib' \
        -DCMAKE_CXX_COMPILER=/usr/bin/g++

In the above example, some custom library paths are specified and the program's run-time search path is modified.
If cmake doesn't detect the correct compiler automatically, you can specify it with ``-DCMAKE_CXX_COMPILER=``.

Having generated the necessary build files with CMake, simply compile the program using ``make`` and move it to the
install location with ``make install``:

::

    $ make -j8           # use 8 cores for compilation
    $ make install       # move the binaries and libraries to the INSTALL_PREFIX path

You are now ready to execute your first simulation.


Building with Intel MKL, Intel compiler (and Intel MPI)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is possible to use the `Intel® Parallel Studio <https://software.intel.com/en-us/parallel-studio-xe>`_
for compilation, which includes the `Intel® Math Kernel Library (MKL) <https://software.intel.com/en-us/mkl>`_
that STEMsalabim can use for discrete fourier transforms instead of FFTW3. If the
`Intel® MPI Library <https://software.intel.com/en-us/intel-mpi-library>`_ is also available, it can be used
for MPI communication.

.. note:: We have tested compiling and running STEMsalabim only with Parallel Studio 2017 so far.

STEMsalabim's CMake files try to find the necessary libraries themselves, when the folling conditions are true:

 1. Either the environment variable :code:`MKLROOT` is set to a valid install location of the MKL, or
    the CMake variable :code:`MKL_ROOT` (pointing at the same location) is specified.
 2. The CMake variable :code:`GCCDIR` points to the install directory of a C++11 compatible GCC compiler.
    This is important, because the :code:`libstdc++` from a GCC install is required for the Intel compilers
    to use modern C++ features.

For example, let's say the Intel suite is installed in :code:`/opt/intel` and we have GCC 6.3 installed in
:code:`/opt/gcc-6.3`, then CMake could be invoked like this: ::

    $ export PATH=$PATH:/opt/intel/... # mpicxx and icpc should be in the path!
    $ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/gcc-6.3/lib64 \
         cmake ../source                                  \
            -DMKL_ROOT=/opt/intel                         \
            -DCMAKE_CXX_COMPILER=icpc                     \
            -DGCCDIR=/opt/gcc-6.3                         \
            -D... more CMAKE arguments as described above.

Depending on how your environment variables are set, you may be able to skip the :code:`LD_LIBRARY_PATH=..` part.
When STEMsalabim is executed, you may again need to specify the library path of the :code:`libstdc++`, using ::

    $ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/gcc-6.3/lib64 mpirun -np ... /path/to/stemsalabim -p ...

.. note:: Some fiddling with paths and environment variables is probably necessary. It may help to know
          basic CMake syntax and have a look at :code:`/path/to/stemsalabim/cmake/FindMKL.cmake` if
          CMake is unable to find something.
