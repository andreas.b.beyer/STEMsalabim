Research done with STEMsalabim
==============================
.. |br| raw:: html

   <br />

2018
----

-  `Composition determination of multinary III/V semiconductors via STEM HAADF multislice
   simulations <https://doi.org/10.1016/j.ultramic.2017.11.002>`_
   L. Duschek, A. Beyer, J. O. Oelerich, K. Volz

2017
----

-  `Surface relaxation of strained Ga(P,As)/GaP heterostructures investigated by HAADF
   STEM <https://dx.doi.org/10.1111/jmi.12622>`_ |br|
   A. Beyer, L. Duschek, J. Belz, J. O. Oelerich, K. Jandieri, K. Volz
-  `Atomic structure of ‘W’-type quantum well heterostructures investigated by aberration-corrected
   STEM <https://dx.doi.org/10.1111/jmi.12647>`_ |br|
   P. Kuekelhan, A. Beyer, C. Fuchs, M.J. Weseloh, S.W. Koch, W. Stolz, K. Volz
-  `Influence of surface relaxation of strained layers on atomic resolution ADF
   imaging <https://doi.org/10.1016/j.ultramic.2017.04.019>`_ |br|
   A. Beyer, L. Duschek, J. Belz, J. O. Oelerich, K. Jandieri, K. Volz
-  `Local Bi ordering in MOVPE grown Ga(As,Bi) investigated by high resolution scanning transmission
   electron microscopy <https://doi.org/10.1016/j.apmt.2016.11.007>`_ |br|
   A. Beyer, N. Knaub, P. Rosenow, K. Jandieri, P. Ludewig, L. Bannow, S. W.Koch, R. Tonner, K. Volz
-  `Influence of surface relaxation of strained layers on atomic
   resolution ADF imaging <https://doi.org/10.1016/j.ultramic.2017.04.019>`_ |br|
   A. Beyer, L. Duschek, J. Belz, J. O. Oelerich, K. Jandieri, K. Volz
-  `STEMsalabim: A high-performance computing cluster friendly code
   for scanning transmission electron microscopy image simulations of
   thin specimens <https://doi.org/10.1016/j.ultramic.2017.03.010>`_ |br|
   J. O. Oelerich, L. Duschek, J. Belz, A. Beyer, S. D. Baranovskii, K.
   Volz