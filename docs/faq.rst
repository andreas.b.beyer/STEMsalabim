Frequently Asked Questions
==========================

.. _name:

What about the name STEMsalabim?
--------------------------------

STEMsalabim stems from the word *Simsalabim* which is known in Germany as what a magician says when casting spells (see
`here <https://de.wiktionary.org/wiki/Simsalabim>`_ for a german dictionary entry). Furthermore, the phrase appears in
the famous German children's song `Auf einem Baum ein Kuckuck
<https://de.wikipedia.org/wiki/Auf_einem_Baum_ein_Kuckuck>`_, where the phrase to our knowledge doesn't really have any
meaning. (The song is about a cuckoo sitting on a tree and being shot by a huntsman...) Most germans will recognize the
word. There is no contextual relation between STEM and the word Simsalabim.

When we first started writing the code we came up with it as a working title and we then simply agreed on keeping it.
