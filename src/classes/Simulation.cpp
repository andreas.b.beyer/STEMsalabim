/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "Simulation.hpp"
#include <thread>
#include "../utilities/initialize.hpp"
#include "../libatomic/Scattering.hpp"
#include "../utilities/timings.hpp"
#include "../libatomic/Plasmons.hpp"

#include "cereal/types/memory.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/tuple.hpp"

using namespace std;
using namespace chrono;
using namespace stemsalabim;


void Simulation::init() {
//    int i = 0;
//    while(0==i)
//        sleep(5);

    high_resolution_clock::time_point init_start = high_resolution_clock::now();

    Params &p = Params::getInstance();

    auto &mpi_env = mpi::Environment::getInstance();

    _gridman = std::make_shared<GridManager>(p.cell());
    _fpman = std::make_shared<FPConfManager>(p.cell());
    _io = std::make_shared<IO>();

    // Read and communicate the mean square displacements of the frozen phonon
    // approximation.
    {
        vector<vector<tuple<double, double, double>>> displacements;
        string serialized_data;
        stringstream ss;
        if(mpi_env.isMaster()) {
            displacements = _io->readDisplacementsFromNCFile();
            cereal::BinaryOutputArchive oarchive(ss);
            oarchive(displacements);
            serialized_data = ss.str();
        }

        mpi_env.broadcast(serialized_data, 0);

        if(mpi_env.isSlave()) {
            ss.str(serialized_data);
            cereal::BinaryInputArchive iarchive(ss);
            iarchive(displacements);
        }

        _fpman->setDisplacements(displacements);
    }

    _gridman->generateGrids();

    if(mpi_env.isMaster())
        printStatus(_gridman);


    if(mpi_env.isMpi())
        output::print("[%s, %d] Hello from %s (rank %d/%d), spawning %d threads.\n",
                      mpi_env.name(),
                      mpi_env.rank(),
                      mpi_env.name(),
                      mpi_env.rank(),
                      mpi_env.size(),
                      p.numberOfThreads());
    else
        output::print("This is a single-node calculation using %d threads.\n", p.numberOfThreads());


    {
        // plasmon stuff. Needs to come after crystal and Gridmanager initialization!
        if(p.plasmonsEnabled()) {
            printMaster("Initializing plasmon scattering functions...\n");
            atomic::Plasmons &plasmons = atomic::Plasmons::getInstance();
            plasmons.populateCache(_gridman, p.cell()->electronDensity());
        }
    }

    // generateSlices must be done before writing anything to the output file, i.e.,
    // before calling io->init()
    generatePropagator();

    // initialize output file if this is master process and write some simulation information
    // to it.
    if(mpi_env.isMaster() && _io->rewriteNCFile()) {
        // As IO should be handled by the master only, we need to initialize
        // the instance only when this is the master process.
        // The crystal instance will be initialized here and then broadcast to
        // tge slaves.
        string config_file_contents;
        _io->initNCFile(_gridman, p.cell());
        {
            NCFile f(p.paramsFileName(), false, true);
            config_file_contents = f.group("params").att("config_file_contents");
        }
        _io->writeParams(_gridman, config_file_contents);
        _io->writeGrids(_gridman);
    }
    _io->initBuffers(_gridman);

    printMaster(output::fmt("Initialization took %s.\n", output::humantime(algorithms::getTimeSince(init_start))));

    initBuffers();
}


void Simulation::run() {
    Params &prms = Params::getInstance();

    // initialize an MPI environment
    auto &mpi_env = mpi::Environment::getInstance();

    // set up some clocks to measure times.
    high_resolution_clock::time_point start_sim, t1;
    start_sim = high_resolution_clock::now();

    // choose parallelization scheme
    bool parallelize_configurations = prms.isAlternativeParallelization();

    if(mpi_env.isMpi() && parallelize_configurations) {
        printMaster(output::fmt("%d configurations are calculated in parallel with %d threads each.\n",
                                mpi_env.size(),
                                prms.numberOfThreads()));
    } else {
        printMaster(output::fmt("Pixels are distributed among %d procs with %d threads each.\n",
                                mpi_env.size(),
                                prms.numberOfThreads()));
    }

    SimulationStateManager state_manager(_gridman, parallelize_configurations);

    // loop over simulations
    for(SimulationState &st: state_manager) {

        // this starts a timer
        st.start();

        _fpman->setConfiguration(st.iteration());

        // if a new defocus was started, do some output stuff.
        if(!st.iconf()) {
            printMaster(st, output::fmt("Starting Defocus %.2fnm.\n", st.defocus()));
        }

        printMaster(st, "Starting configuration. \n");

        t1 = high_resolution_clock::now();

        // in each configuration, assign atoms to the corresponding slices.
        _fpman->assignAtomsToSlices(_gridman);

        printMaster(st, output::fmt("Finished slicing in %s.\n", output::humantime(algorithms::getTimeSince(t1))));

        for(shared_ptr<Slice> &slice: _gridman->slices())
            printMaster(st,
                        output::fmt("Slice %d [%.3fnm - %.3fnm] %d atoms.\n",
                                    slice->id(),
                                    slice->z(),
                                    (slice->z() + slice->thickness()),
                                    slice->numberOfAtoms()));

        // in each frozen phonon iteration, first write the crystal to the output
        // file.
        if(mpi_env.isMaster() && _io->rewriteNCFile())
            _io->writeCrystal(_fpman, prms.cell(), _gridman);

        // generate transmission functions in parallel using both threads and the main thread.
        // This is executed on each MPI processor.
        t1 = high_resolution_clock::now();

        if(!prms.storedPotentials()) {
            TaskQueue slice_work;
            slice_work.append(_gridman->slices());


            vector<thread> threads;
            for(unsigned int nt = 0; nt < prms.numberOfThreads(); nt++) {
                threads.emplace_back([&] {
                    unsigned int slic_index;
                    while(slice_work.pop(slic_index)) {
                        _gridman->slices()[slic_index]->calculateTransmissionFunction(_fpman, _gridman);
                        slice_work.finish(slic_index);
                    }
                });
            }

            slice_work.waitUntilFinished();
            for(auto &t : threads)
                t.join();
        } else {
            for(auto &slic: _gridman->slices()) {
                slic->calculateTransmissionFunction(_gridman, _io->readStoredPotential(_fpman, slic->id()));
            }
        }

        printLine(st,
                  output::fmt("Finished transmission functions in %s.\n",
                              output::humantime(algorithms::getTimeSince(t1))));

        _io->generateTemporaryResultFile();

        if(mpi_env.isMaster() || parallelize_configurations) {
            multisliceMaster(st, !parallelize_configurations);
        } else {
            multisliceWorker(st);
        }

        // each process now (sequentially) writes his pixels to the NC file.

        mpi_env.barrier();

        auto start_io = high_resolution_clock::now();

        // note that when configurations are run in parallel, it is crucial that rank 0 writes to the output
        // NC file first, because otherwise the averaging of results will fail.
        for(int rank = 0; rank < mpi_env.size(); rank++) {
            if(rank == mpi_env.rank()) {
                auto start = high_resolution_clock::now();
                _io->copyFromTemporaryFile(_gridman, _com_buffer, _adf_intensity_buffer, _cbed_intensity_buffer);
                _io->removeTemporaryResultFile();
                printLine(st,
                          output::fmt("Finished copying pixels for %s.\n",
                                      output::humantime(algorithms::getTimeSince(start))));
            }

            mpi_env.barrier();
        }

        printMaster(st,
                    output::fmt("Gathering result files took %s.\n",
                                output::humantime(algorithms::getTimeSince(start_io))));
    };

    if(mpi_env.isMaster()) {
        _io->writeRuntime();
    }

    printMaster(output::fmt("Finished computation in %s. Find the results in %s.\n",
                            output::humantime(algorithms::getTimeSince(start_sim)),
                            prms.outputFilename()));

}


seconds Simulation::calculatePixel(ScanPoint &point, double defocus) {
    auto start_time = high_resolution_clock::now();

    Params &p = Params::getInstance();
    atomic::Plasmons &plasmons = atomic::Plasmons::getInstance();

    vector<float> adf_intensities;
    vector<float> center_of_mass;
    vector<float> cbed_intensities;
    vector<float> detector_intensities;

    if(point.adf) {
        adf_intensities.reserve(_gridman->adfSliceCoords().size() *
                                _gridman->adfDetectorGrid().size() *
                                _gridman->energyLossGrid().size());
        center_of_mass.reserve(_gridman->adfSliceCoords().size() * _gridman->energyLossGrid().size() * 2);
        detector_intensities.resize(_gridman->adfDetectorGrid().size());
    }

    if(point.cbed)
        cbed_intensities.reserve(_gridman->storedCbedSizeX() *
                                 _gridman->storedCbedSizeY() *
                                 _gridman->cbedSliceCoords().size() *
                                 _gridman->energyLossGrid().size());

    std::vector<Wave> waves(_gridman->energyLossGrid().size());

    Wave tmpwave;
    if(p.plasmonsEnabled()) {
        tmpwave.init(_gridman->samplingX(), _gridman->samplingY());
    }

    for(auto &c: waves)
        c.init(_gridman->samplingX(), _gridman->samplingY());

    // shape the probe wave function according to where the probe is
    makeProbe(waves[0], defocus, point.x, point.y);

    // propagate through each slice and the following vacuum
    for(const shared_ptr<Slice> &slice: _gridman->slices()) {

        // zero loss
        if(p.plasmonsEnabled()) {

            // for now, single scattering only...
//            for(unsigned int energy_i = _gridman->energyLossGrid().size() - 1u; energy_i > 0; energy_i--) {
//
//                waves[energy_i] *= sqrt(plasmons.beta0());
//
//                tmpwave = waves[0];
//                tmpwave *= plasmons.scatteringFunction(energy_i - 1);
//                tmpwave *= sqrt(plasmons.beta1());
//                waves[energy_i] += tmpwave;
//            }
//
            // scattering
            for(unsigned int energy_i = _gridman->energyLossGrid().size() - 1u; energy_i > 0; energy_i--) {

                waves[energy_i] *= sqrt(plasmons.beta0());

                // plural scattering includes single scattering with energy_j = 0
                for(unsigned int energy_j = 0; energy_j < (p.plasmonsPluralScattering() ? energy_i : 1u); energy_j++) {
                    tmpwave = waves[energy_j];
                    tmpwave *= plasmons.scatteringFunction(energy_i - energy_j - 1);
                    tmpwave *= sqrt(plasmons.beta1());
                    waves[energy_i] += tmpwave;
                }
            }
//

            waves[0] *= sqrt(plasmons.beta0());

        }

        for(auto &c: waves)
            slice->transmit(c);

        // the propagate function with the false as second parameter
        // leaves the wave in fourier space, so we can apply detector without
        // FFTing the wave back and forth.
        // Also, propagation takes care of bandwidth limiting the wave function
        // to 2/3 of the maximum k.
        for(auto &c: waves)
            propagate(c, false);

        // we are now in fourier space, so let's normalize the inelastic scattered wave functions
        if(p.plasmonsEnabled()) {

            double total_intensity = 0;
            double desired_intensity = 1 - waves[0].integratedIntensity();

            for(unsigned long ei = 1; ei < _gridman->energyLossGrid().size(); ei++)
                total_intensity += waves[ei].integratedIntensity();

            for(unsigned long ei = 1; ei < _gridman->energyLossGrid().size(); ei++)
                waves[ei] *= sqrt(desired_intensity / total_intensity);

        } else if(p.normalizeAlways()) {
            waves[0].normalize();
        }

        if(point.adf && _gridman->adfStoreSlice(slice->id())) {

            // here, we iterate the k space and calculate the wave angles.
            // Then, we determine the corresponding detector angle and sum the intensity up.
            for(unsigned int ip = 0; ip < _gridman->energyLossGrid().size(); ip++) {
                std::fill(detector_intensities.begin(), detector_intensities.end(), 0);

                for(unsigned int ix = 0; ix < _gridman->samplingX(); ix++) {
                    for(unsigned int iy = 0; iy < _gridman->samplingY(); iy++) {

                        int ind = _gridman->adfBinIndex(ix, iy);

                        if(ind >= 0) {
                            // check for not a number
                            auto intens = (float) pow(abs(waves[ip](ix, iy)), 2);
                            if(!::isnan(intens))
                                detector_intensities[ind] += intens;
                        }
                    }
                }


                adf_intensities.insert(adf_intensities.end(), detector_intensities.begin(), detector_intensities.end());

                // calculate center of mass
                double total_intensity = waves[ip].integratedIntensity(), cx = 0, cy = 0;
                for(unsigned int ix = 0; ix < _gridman->samplingX(); ix++) {
                    for(unsigned int iy = 0; iy < _gridman->samplingY(); iy++) {
                        auto intens = (float) pow(abs(waves[ip](ix, iy)), 2);
                        cx += intens * _gridman->kx(ix);
                        cy += intens * _gridman->ky(iy);
                    }
                }
                cx /= total_intensity;
                cy /= total_intensity;

                center_of_mass.push_back((float) (cx * p.wavelength() * 1e3));
                center_of_mass.push_back((float) (cy * p.wavelength() * 1e3));
            }



        }

        if(point.cbed && _gridman->cbedStoreSlice(slice->id())) {

            // first, we cut off high frequencies from the wave if necessary. Note, that we follow
            // the FFTW storage scheme for frequencies, hence, half of the values are in the lower
            // range and the other half in the range of the wave function.
            size_t bandwidth_lx = _gridman->storedCbedSizeX(true);
            size_t bandwidth_ly = _gridman->storedCbedSizeY(true);

            vector<float> tmp_intensities;
            tmp_intensities.resize(bandwidth_lx * bandwidth_ly);

            for(unsigned int ip = 0; ip < _gridman->energyLossGrid().size(); ip++) {

                double total_intensity = 0, intensity;

                unsigned int index_count = 0;
                for(size_t i = 0; i < bandwidth_lx; i++) {
                    size_t ii = i;

                    if(i > bandwidth_lx / 2)
                        ii = waves[ip].lx() - bandwidth_lx + i;

                    for(size_t j = 0; j < bandwidth_ly; j++) {
                        size_t jj = j;

                        if(j > bandwidth_ly / 2)
                            jj = waves[ip].ly() - bandwidth_ly + j;

                        intensity = pow(abs(waves[ip](ii, jj)), 2);
                        total_intensity += intensity;
                        tmp_intensities[index_count++] = intensity;
                    }
                }

                // take care of resizing the CBED if desired
                if(p.cbedRescale()) {
                    vector<float> rescaled;
                    rescaled = algorithms::bilinearRescale(tmp_intensities,
                                                           p.cbedSizeX(),
                                                           p.cbedSizeY(),
                                                           bandwidth_lx,
                                                           bandwidth_ly);

                    double int_sum_rescaled = 0;
                    for(float i : rescaled)
                        int_sum_rescaled += i;

                    for(float &i : rescaled)
                        i *= total_intensity / int_sum_rescaled;

                    cbed_intensities.insert(cbed_intensities.end(), rescaled.begin(), rescaled.end());
                } else {
                    cbed_intensities.insert(cbed_intensities.end(), tmp_intensities.begin(), tmp_intensities.end());
                }
            }

        }
        for(auto &c: waves) {
            c.backwardFFT();
        }
    }

    if(point.adf) {
        point.storeAdfIntensities(adf_intensities, _adf_intensity_buffer);
        point.storeCOM(center_of_mass, _com_buffer);
    }

    if(point.cbed)
        point.storeCBEDIntensities(cbed_intensities, _cbed_intensity_buffer);

    auto finish_time = high_resolution_clock::now();
    return duration_cast<seconds>(finish_time - start_time);

}

void Simulation::printLine(const SimulationState &st, const string &line) {
    Params &p = Params::getInstance();

    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isMpi())
        output::print("[%d/%d, %d/%d] [rank %d] %s",
                      st.idefocus() + 1,
                      p.numberOfDefoci(),
                      st.iconf() + 1,
                      p.numberOfConfigurations(),
                      mpi_env.rank(),
                      line);
    else
        output::print("[%d/%d, %d/%d] %s",
                      st.idefocus() + 1,
                      p.numberOfDefoci(),
                      st.iconf() + 1,
                      p.numberOfConfigurations(),
                      line);
}


void Simulation::printMaster(const SimulationState &st, const string &line) {
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isMaster())
        printLine(st, line);
}


void Simulation::printMaster(const string &line) {
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isMaster())
        output::print("%s", line);
}

void Simulation::storeResultData(unsigned int idefocus, unsigned int iconf, const vector<unsigned int> &pix_ids,
        vector<ScanPoint> &work_packages) {
    for(auto px_idx : pix_ids) {
        ScanPoint &sp = work_packages[px_idx];

        // we store only pixels with intensities, as all others are the ones
        // which are calculated by a different MPI worker.
        if(sp.hasAdfIntensities() || sp.hasCbedIntensities())
            IO::writeTemporaryResult(idefocus, iconf, sp, _com_buffer, _adf_intensity_buffer, _cbed_intensity_buffer);
    }
}

void Simulation::multisliceMaster(const SimulationState &st, bool do_mpi_queue) {

    Params &prms = Params::getInstance();

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    // initialize an MPI environment
    auto &mpi_env = mpi::Environment::getInstance();

    // This vector<float> holds all pixels/scan points of the STEM image to be calculated.
    vector<ScanPoint> work_packages = _gridman->scanPoints();

    // This task queue holds all indices of the pixels to calculate. It'll be needed only
    // on the MPI master, as each MPI slave will manage its own queue of tasks.
    TaskQueue scan_work;
    scan_work.append(work_packages);

    atomic_ulong px_counter = 0;
    atomic_ullong px_calc_seconds = 0;

    // keep the master's worker threads busy as well. We will fetch from the task queue
    // one by one AFTER the previous task is finished, as the queue is worked on in parallel
    // via MPI. It is very important not to distribute all tasks here.
    std::vector<std::thread> threads;
    for(unsigned int nt = 1; nt < prms.numberOfThreads(); nt++) {
        threads.emplace_back([&] {
            unsigned int pix_index = 0;
            while(scan_work.pop(pix_index)) {
                px_counter++;
                auto duration = calculatePixel(work_packages[pix_index], st.defocus());
                px_calc_seconds += duration.count() / px_counter;
                scan_work.finish(pix_index);
            }
        });
    }

    // now, wait for incoming results and, upon receiving one, send a new work package out if
    // there are any left
    if(mpi_env.isMpi() && mpi_env.isMaster() && do_mpi_queue) {

        mpi::Status s;

        unsigned int last_prcnt = 0;

        // We loop here until a valid MPI request comes in. When it does, process the results and
        // send out a new work package.
        do {
            if(mpi_env.iprobe(s)) {
                if(s.tag() == mpi::Environment::MPI_TAG_TASKS_REQUEST) {
                    vector<unsigned int> package = scan_work.popMultiple(prms.workPackageSize());
                    vector<unsigned int> finished_pixels;

                    mpi_env.recv(finished_pixels, s.source(), s.tag());
                    mpi_env.send(package, s.source(), mpi::Environment::MPI_TAG_TASKS);

                    scan_work.finish(finished_pixels);
                }

            }

            storeResultData(st.idefocus(), st.iconf(), scan_work.getDirtyAndClean(), work_packages);

            // progress output
            float p = scan_work.progress();
            if(p * 100 > last_prcnt + 10) {
                last_prcnt = (unsigned int) ceil(p * 100);

                printLine(st,
                          output::fmt("progr: %d%%, eta/conf: %s\n", ceil(p * 100), output::humantime(st.etaConf(p))));
            }
        } while(!scan_work.finished());

        scan_work.waitUntilFinished();

        storeResultData(st.idefocus(), st.iconf(), scan_work.getDirtyAndClean(), work_packages);

        printLine(st,
                  output::fmt("Received all results, calculation took %s.\n",
                              output::humantime(algorithms::getTimeSince(t1))));

    } else {

        // do some more work on the main thread.
        unsigned int pix_index = 0;
        unsigned int last_prcnt = 0;

        while(scan_work.pop(pix_index)) {
            px_counter++;
            auto duration = calculatePixel(work_packages[pix_index], st.defocus());
            px_calc_seconds += duration.count() / px_counter;
            scan_work.finish(pix_index);

            // store finished intensities
            storeResultData(st.idefocus(), st.iconf(), scan_work.getDirtyAndClean(), work_packages);

            float p = scan_work.progress();

            if(p * 100 > last_prcnt + 10) {
                last_prcnt = (unsigned int) ceil(p * 100);

                printLine(st,
                          output::fmt("progr: %d%%, eta/conf: %s\n", ceil(p * 100), output::humantime(st.etaConf(p))));
            }
        }

        //io_duration += storeDirtyResults(st, work_packages, scan_work);
        scan_work.waitUntilFinished();

        storeResultData(st.idefocus(), st.iconf(), scan_work.getDirtyAndClean(), work_packages);

        printLine(st, output::fmt("Calculation took %s\n", output::humantime(algorithms::getTimeSince(t1))));
    }

    // join the threads
    for(auto &t: threads) {
        t.join();
    }


}


void Simulation::multisliceWorker(const SimulationState &st) {
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    // initialize an MPI environment
    auto &mpi_env = mpi::Environment::getInstance();
    auto &prms = Params::getInstance();

    if(mpi_env.isMaster())
        output::error("This function cannot be called by the MPI master!");

    // This vector<float> holds all pixels/scan points of the STEM image to be calculated.
    vector<ScanPoint> work_packages = _gridman->scanPoints();

    // set up own work queue with pixel indices
    TaskQueue pixel_index_queue;
    unsigned long pixel_calculated = 0;
    vector<unsigned int> package;
    vector<thread> threads;

    while(true) {

        // request new tasks
        mpi_env.send(package, mpi::Environment::MASTER, mpi::Environment::MPI_TAG_TASKS_REQUEST);
        mpi_env.recv(package, 0, mpi::Environment::MPI_TAG_TASKS);

        if(package.empty())
            break;

        pixel_index_queue.append(package);

        pixel_calculated += package.size();

        for(unsigned int nt = 1; nt < prms.numberOfThreads(); nt++) {
            threads.emplace_back([&] {
                unsigned int pix_index;
                while(pixel_index_queue.pop(pix_index)) {
                    calculatePixel(work_packages[pix_index], st.defocus());
                    pixel_index_queue.finish(pix_index);
                }
            });
        }

        // also do work on the main thread.
        unsigned int pix_index;
        while(pixel_index_queue.pop(pix_index)) {
            calculatePixel(work_packages[pix_index], st.defocus());
            pixel_index_queue.finish(pix_index);
        }

        pixel_index_queue.waitUntilFinished();

        for(auto &t: threads)
            t.join();
        threads.clear();

        pixel_index_queue.reset();

        storeResultData(st.idefocus(), st.iconf(), package, work_packages);

    }

    printLine(st,
              output::fmt("Finished working for %s and calculated %d pixels.\n",
                          output::humantime(algorithms::getTimeSince(t1)),
                          pixel_calculated));
}


void Simulation::initBuffers() {
    Params &prms = Params::getInstance();
    typedef typename memory::buffer::number_buffer<float> buf_type;

    // The serialization buffer is for sending work packages around. It should be larger than the
    // theoretical maximal size of any work package filled with data.
    unsigned long number_intensities_per_pixel = prms.adf() ? (_gridman->adfDetectorGrid().size() *
                                                               _gridman->adfSliceCoords().size() *
                                                               _gridman->energyLossGrid().size()) : 0;
    unsigned long number_cbed_per_pixel = prms.cbed() ? (_gridman->storedCbedSizeX() *
                                                         _gridman->storedCbedSizeY() *
                                                         _gridman->cbedSliceCoords().size() *
                                                         _gridman->energyLossGrid().size()) : 0;

    _com_buffer = std::make_shared<buf_type>(prms.adf() ? (2 *
                                                           _gridman->adfSliceCoords().size() *
                                                           _gridman->energyLossGrid().size()) : 0,
                                             prms.workPackageSize());
    _adf_intensity_buffer = std::make_shared<buf_type>(number_intensities_per_pixel, prms.workPackageSize());
    _cbed_intensity_buffer = std::make_shared<buf_type>(number_cbed_per_pixel, prms.workPackageSize());
}


void Simulation::generatePropagator() {
    Params &p = Params::getInstance();

    double scale = -1. * Params::pi * p.sliceThickness() * p.wavelength();


    unsigned int lx = _gridman->samplingX();
    unsigned int ly = _gridman->samplingY();
    double sdense2_x = pow(1. / 3. * _gridman->densityX(), 2);
    double sdense2_y = pow(1. / 3. * _gridman->densityY(), 2);

    _propagator.init(lx, ly);
    _propagator.setIsKSpace(true);

    for(unsigned int ix = 0; ix < lx; ix++) {
        for(unsigned int iy = 0; iy < ly; iy++) {
            if(!p.bandwidthLimiting() ||
               pow(_gridman->kx(ix), 2) / sdense2_x + pow(_gridman->ky(iy), 2) / sdense2_y < 1) {
                _propagator(ix, iy) = polar(1.0, scale * (float) (pow(_gridman->kx(ix), 2) + pow(_gridman->ky(iy), 2)));
            } else {
                _propagator(ix, iy) = 0.0;
            }
        }
    }


}


void Simulation::propagate(Wave &wave, bool do_backward_fft) {
    // propagate and bandwidth limit
    // see page 145ff in Kirkland and 402 - 405 in autostem.cpp of his code.
    wave.forwardFFT();

    wave *= _propagator;

    if(do_backward_fft)
        wave.backwardFFT();
}


void Simulation::makeProbe(Wave &wave, const double defocus, double px, double py) const {
    Params &p = Params::getInstance();

    double wl = p.wavelength();
    double alpha_sq;
    double kx, ky;
    double chi_defocus, chi_astigmatism, chi_cs, chi_c5;
    double pref = Params::pi / wl;
    double w;

    wave.setIsKSpace(true);

    for(unsigned int x = 0; x < _gridman->samplingX(); ++x) {
        for(unsigned int y = 0; y < _gridman->samplingY(); ++y) {
            kx = _gridman->kx(x);
            ky = _gridman->ky(y);
            alpha_sq = wl * wl * (kx * kx + ky * ky);

            if(alpha_sq <= pow(p.probeMinAperture() / 1000., 2) || alpha_sq >= pow(p.probeMaxAperture() / 1000., 2)) {

                // set wave to zero here.
                wave(x, y) = std::polar(0.0, 1.0);
            } else {
                chi_defocus = -pref * defocus * alpha_sq;
                chi_astigmatism = pref *
                                  alpha_sq *
                                  p.probeAstigmatismCa() *
                                  (double) cos(2 * (atan2(ky, kx) - p.probeAstigmatismAngle()));
                chi_cs = pref / 2 * p.probePhericalAberrationCoeff() * (double) pow(alpha_sq, 2);
                chi_c5 = pref / 3 * p.probeC5() * (double) pow(alpha_sq, 3);

                w = 2.0 * Params::pi * (px * kx + py * ky);
                wave(x, y) = std::polar(1.0, w + chi_defocus - chi_astigmatism - chi_cs - chi_c5);
            }
        }
    }

    wave.normalize();
    wave.backwardFFT();
}
