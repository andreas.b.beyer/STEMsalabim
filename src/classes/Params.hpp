#include <utility>

/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_PARAMS_HPP
#define STEMSALABIM_PARAMS_HPP

#include <string>
#include <tuple>
#include <libconfig.h++>
#include <random>
#include <cmath>
#include <algorithm>

#include "../libatomic/Cell.hpp"

namespace stemsalabim {

    /*!
     * A singleton class containing all simulation parameters, which are collected from command line
     * arguments and the input parameter file. In addition, it generates (if required) a random seed
     * as well as the simulation UUID.
     */
    class Params {
    public:
        /*!
         * Return the instance of the class, which is created if called for the first time.
         */
        static Params &getInstance() {
            static Params instance;
            return instance;
        }

        /*!
         * Initialize the parameters that are read from the command line, including most importantly
         * the location of the parameter file.
         */
        void initFromCLI(int argc, const char **argv);

        /*!
         * Make the Params class know the cell...
         */
        void setCell(const std::shared_ptr<atomic::Cell> &cell) {
            _cell = cell;
        }

        /*!
         * Access to the cell associated with params class.
         */
        std::shared_ptr<atomic::Cell> cell() const {
            return _cell;
        }

        /*!
         * Return the generated universally unique identifier (UUID) of this simulation.
         */
        const std::string &uuid() const {
            return _uuid;
        }

        /*!
         * Return the command line arguments string, i.e., the command line call
         * minus the executable name.
         */
        const std::string &cliArguments() const {
            return _command_line_arguments;
        }

        /*!
         * Return the file path to the parameters file.
         */
        const std::string &paramsFileName() const {
            return _param_file;
        };

        /*!
         * Set the param file path.
         */
        void setParamsFileName(const std::string & path) {
            _param_file = path;
        };

        /*!
         * Read the simulation parameters from a string with the contents of the parameters file.
         * The file is not read in by the Param class itself to ensure that MPI processes other than the
         * master don't do IO.
         */
        void readParamsFromString(const std::string &prms);

        /*!
         * Read the simulation parameters from another NC file.
         */
        void readParamsFromNCFile(const std::string & path);
        void readParamsFromNCFile();

        /*!
         * broadcast parameters to other MPI procs.
         */
        void broadcast(int rank);

        /*!
         * Return the path to the crystal XYZ file.
         */
        const std::string &crystalFilename() const {
            return _crystal_file;
        }

        /*!
         * Return path to the output file name.
         */
        const std::string &outputFilename() const {
            return _output_file;
        }

        /*!
         * Return path to the temporary directory. If empty string, use the
         * dir of the output file.
         */
        const std::string &tmpDir() const {
            return _tmp_dir;
        }

        /*!
         * Returns if output should be compressed.
         */
        bool outputCompress() const {
            return _output_compress;
        }

        /*!
         * Returns if potentials should be stored and read from the init file.
         */
        bool storedPotentials() const {
            return _stored_potentials;
        }

        /*!
         * Returns if simulation was initialized from NC file.
         */
        bool initFromNCFile() const {
            return _init_from_ncfile;
        }

        /*!
         * Return the title of the simulation.
         */
        const std::string &title() const {
            return _title;
        }

        /*!
         * Return the Slice thickness of the multi-slice algorithm.
         */
        double sliceThickness() const {
            return _slice_thickness;
        }

        /*!
         * Return the cutoff radius of the atomic potentials.
         */
        double maxPotentialRadius() const {
            return _max_potential_radius;
        }

        /*!
         * Return the beam energy of the microscope.
         */
        double beamEnergy() const {
            return _beam_energy;
        }

        /*!
         * Get the density of probe scan sampling in points/nm.
         */
        double probeDensity() const {
            return _probe_scan_density;
        }

        /*!
         * Return the max. numeric aperture of the electron probe.
         */
        double probeMaxAperture() const {
            return _probe_max_aperture;
        }

        /*!
         * Return the min. numeric aperture of the electron probe.
         */
        double probeMinAperture() const {
            return _probe_min_aperture;
        }

        /*!
         * Return the fifth order spherical aberration coefficient of the microscope.
         */
        double probeC5() const {
            return _probe_c5_coeff;
        }

        /*!
         * Return the third order spherical aberration coefficient of the microscope.
         */
        double probePhericalAberrationCoeff() const {
            return _probe_spherical_aberration_coeff;
        }

        /*!
         * Get the FWHM part of the defocus information tuple.
         */
        double fwhmDefocus() const {
            return _probe_fwhm_defoci;
        }

        /*!
         * Get the center part of the defocus information tuple.
         */
        double meanDefocus() const {
            return _probe_defocus;
        }

        /*!
         * Get the number part of the defocus information tuple.
         */
        unsigned int numberOfDefoci() const {
            return _probe_num_defoci;
        }

        /*!
         * Return two-fold astigmatism coefficient.
         */
        double probeAstigmatismCa() const {
            return _probe_astigmatism_ca;
        }

        /*!
         * Return two-fold astigmatism angle coefficient.
         */
        double probeAstigmatismAngle() const {
            return _probe_astigmatism_angle;
        }

        /*!
         * Get the number of frozen phonon configurations.
         */
        unsigned int numberOfConfigurations() const {
            return _number_configurations;
        }

        /*!
         * Return whether frozen phonon should be enabled
         */
        bool fpEnabled() const {
            return _fp_enabled;
        }


        /*!
         * Get the (generated) random seed of this simulation.
         */
        unsigned int randomSeed() const {
            return _random_seed;
        }

        /*!
         * Get the density of real and phase space grating sampling in points/nm.
         */
        double samplingDensity() const {
            return _sample_density;
        }

        /*!
         * Get the ranom number generator.
         */
        const std::mt19937 &getRandomGenerator() const {
            return _rgen;
        }

        /*!
         * Return whether slicing is fixed, i.e., the z coordinates of the Atom s are not
         * varied in FP vibrations.
         */
        bool isFixedSlicing() const {
            return _fixed_slicing;
        }

        /*!
         * Return whether wave functions are bandwidth limited.
         */
        bool bandwidthLimiting() const {
            return _bandwidth_limiting;
        }

        /*!
         * Return whether wave functions are renormalized after each slice.
         */
        bool normalizeAlways() const {
            return _normalize_always;
        }

        /*!
         * Return the package of each thread of slave MPI processes,
         * i.e., how many pixels this thread calculates before reporting the results back to the master.
         */
        unsigned int workPackageSize() const {
            return _work_package_size;
        }

        /*!
         * Return the wavelength of the electron beam in angstroms.
         */
        double wavelength() const {
            return PLANCK_CONSTANT_SPEED_LIGHT / sqrt(_beam_energy * (2 * ELECTRON_REST_MASS + _beam_energy));
        }

        /*!
         * Return the \f$\gamma\f$ factor of the electron beam.
         */
        double gamma() const {
            return (1 + _beam_energy / ELECTRON_REST_MASS);
        }

        /*!
         * Return the number of threads per MPI process.
         */
        unsigned int numberOfThreads() const {
            return _num_threads;
        }

        /*!
         * Returns true if any ADF is to be saved at all.
         */
        bool adf() const {
            return _adf_enabled;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the STEM probe in x direction.
         */
        const std::tuple<double, double> &adfScanX() const {
            return _adf_scan_x;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the STEM probe in y direction.
         */
        const std::tuple<double, double> &adfScanY() const {
            return _adf_scan_y;
        }

        /*!
         * Get the detector angles interval and number, as a tuple containing
         * min detector angle, max detector angle, number of points.
         */
        const std::tuple<double, double, unsigned int> &adfDetectorAngles() const {
            return _adf_detector_angles;
        }

        /*!
         * Get the number of detector angles to calculate intensities for.
         */
        unsigned int adfNumberOfDetectorAngles() const {
            return std::get<2>(_adf_detector_angles);
        }

        /*!
         * Return the periodicity of slices after which intensities are written to the output file.
         * 0 - Only after the bottom of the sample
         * 1 - after each slice
         * N - after every Nth slice
         */
        unsigned int adfSaveEveryNSlices() const {
            return _adf_save_every_n_slices;
        }

        /*!
         * Get the exponent of the detector interval sizes.
         * 1 - linear interval (default)
         * > 1 - adaptive interval (smaller angles -> finer sampling)
         */
        float adfDetectorIntervalExponent() const {
            return _adf_detector_interval_exponent;
        }

        /*!
         * Return whether FP configurations should be averaged in the output file.
         */
        bool adfAverageConfigurations() const {
            return _adf_average_configurations;
        }

        bool isAlternativeParallelization() const {
            return _alternative_parallelization;
        }

        /*!
         * When some fixed grid size is given, return true
         */
        bool isFixedGrid() const {
            return _nkx > 0 && _nky > 0;
        }

        unsigned int fixedGridX() const {
            return _nkx;
        }

        unsigned int fixedGridY() const {
            return _nky;
        }

        /*!
         * Return whether different defoci simulations are averaged in the output file.
         */
        bool adfAverageDefoci() const {
            return _adf_average_defoci;
        }

        /*!
         * Returns true if any CBED is to be saved at all.
         */
        bool cbed() const {
            return _cbed_enabled;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the CBEDs in x direction.
         */
        const std::tuple<double, double> &cbedScanX() const {
            return _cbed_scan_x;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the CBEDs in y direction.
         */
        const std::tuple<double, double> &cbedScanY() const {
            return _cbed_scan_y;
        }

        /*!
         * Get the desired size of the CBED images to store. Will return a tuple (0,0) if
         * not set as a parameter.
         */
        const std::tuple<unsigned int, unsigned int> &cbedSize() const {
            return _cbed_size;
        }

        /*!
         * Get the desired size of the CBED images to store in X direction. Will return 0 if
         * not set as a parameter.
         */
        unsigned int cbedSizeX() const {
            return std::get<0>(_cbed_size);
        }

        /*!
         * Get the desired size of the CBED images to store in Y direction. Will return 0 if
         * not set as a parameter.
         */
        unsigned int cbedSizeY() const {
            return std::get<1>(_cbed_size);
        }

        /*!
         * Returns true if the desired CBED size is not 0,0, in which case the CBED
         * is to be rescaled before storage.
         */
        bool cbedRescale() const {
            return cbedSizeX() > 0 && cbedSizeY() > 0;
        }

        /*!
         * Return whether CBED FP configurations should be averaged in the output file.
         */
        bool cbedAverageConfigurations() const {
            return _cbed_average_configurations;
        }

        /*!
         * Return whether different CBED defoci simulations are averaged in the output file.
         */
        bool cbedAverageDefoci() const {
            return _cbed_average_defoci;
        }

        /*!
         * Return the periodicity of slices after which CBEDs are written to the output file.
         * 0 - Only after the bottom of the sample
         * 1 - after each slice
         * N - after every Nth slice
         */
        unsigned int cbedSaveEveryNSlices() const {
            return _cbed_save_every_n_slices;
        }

        /*!
         * Return true if plasmon scattering is enabled.
         */
        bool plasmonsEnabled() const {
            return _plasmons_enabled;
        }

        /*!
         * Return true when the simple plasmon mode is activated
         */
        bool plasmonsSimple() const {
            return _plasmons_simple;
        }

        /*!
         * Return true when plural plasmon scattering is enabled
         */
        bool plasmonsPluralScattering() const {
            return _plasmons_plural;
        }

        /*!
         * Return the maximal energy for the plasmon energy grid
         */
        float plasmonsMaxEnergy() const {
            return _plasmons_max_energy;
        }

        /*!
         * Return the density in 1/eV for the plasmon energy grid.
         */
        float plasmonsEnergyGridDensity() const {
            return _plasmons_energy_grid_density;
        }

        /*!
         * Plasmon mean free path in nm
         */
        float plasmonMeanFreePath() const {
            return _plasmons_mean_free_path;
        }

        /*!
         * Plasmon energy in eV
         */
        float plasmonEnergy() const {
            return _plasmons_energy;
        }

        /*!
         * Plasmon energy full with half maximum in eV
         */
        float plasmonFWHM() const {
            return _plasmons_fwhm;
        }

        /// electron rest mass in keV. \f$m_0  c^2\f$ in keV
        constexpr static double ELECTRON_REST_MASS = 510.99906;

        /// product of planck's constant and speed of light in keV * nm.
        constexpr static double PLANCK_CONSTANT_SPEED_LIGHT = 1.239841875;

        /// pi. Approximately.
        constexpr static double pi = 3.14159265358979323846;

        /// pi. Approximately.
        constexpr static double e = 1.602176462e-19;

        std::string _param_file{""};
    private:
        Params() = default;

        std::string _command_line_arguments{""};

        std::string _uuid{""};

        double _sample_density{360};
        bool _output_compress{false};

        bool _init_from_ncfile{false};

        double _probe_max_aperture{24.0};
        double _probe_min_aperture{0.0};
        double _probe_c5_coeff{5e6};
        double _probe_spherical_aberration_coeff{2e3};
        double _probe_defocus{-2};
        unsigned int _probe_num_defoci{1};
        double _probe_fwhm_defoci{6};
        double _probe_astigmatism_angle{0};
        double _probe_astigmatism_ca{0};
        double _probe_scan_density{40}; // in A^-1

        double _beam_energy{200.0};

        double _max_potential_radius{0.3};

        double _slice_thickness{0.2715};

        unsigned int _number_configurations{1};
        bool _fp_enabled{true};
        bool _fixed_slicing{true};

        std::string _crystal_file{""};
        std::string _output_file{""};
        std::string _tmp_dir{"."};
        std::string _title{""};

        bool _bandwidth_limiting{true};
        bool _normalize_always{false};

        bool _stored_potentials{false};

        unsigned int _random_seed{0};
        std::mt19937 _rgen{0};
        unsigned _work_package_size{10};
        unsigned _num_threads{1};
        bool _alternative_parallelization{false};

        // Settings related to ADF mode
        bool _adf_enabled{true};
        std::tuple<double, double> _adf_scan_x{std::make_tuple(0.0, 1.0)};
        std::tuple<double, double> _adf_scan_y{std::make_tuple(0.0, 1.0)};
        unsigned int _adf_save_every_n_slices{1};
        std::tuple<double, double, unsigned int> _adf_detector_angles{std::make_tuple(0.0, 300.0, 301)};
        float _adf_detector_interval_exponent{1.0};
        bool _adf_average_configurations{true};
        bool _adf_average_defoci{true};

        // settings related to CBED mode
        bool _cbed_enabled{false};
        std::tuple<double, double> _cbed_scan_x{std::make_tuple(0.0, 1.0)};
        std::tuple<double, double> _cbed_scan_y{std::make_tuple(0.0, 1.0)};
        std::tuple<unsigned int, unsigned int> _cbed_size{std::make_tuple(0, 0)};
        unsigned int _cbed_save_every_n_slices{0};
        bool _cbed_average_configurations{true};
        bool _cbed_average_defoci{true};

        // plasmon scattering
        bool _plasmons_enabled{false};
        bool _plasmons_simple{false};
        bool _plasmons_plural{false};
        float _plasmons_max_energy{10};
        float _plasmons_energy_grid_density{10};
        float _plasmons_mean_free_path{110};
        float _plasmons_energy{16.6};
        float _plasmons_fwhm{3.7};

        libconfig::Config _cfg;

        std::vector<std::string> _cli_options;

        bool cliParamGiven(const std::string &p) {
            return std::find(_cli_options.begin(), _cli_options.end(), p) != _cli_options.end();
        }

        std::shared_ptr<atomic::Cell> _cell;

        unsigned int _nkx{0};
        unsigned int _nky{0};

    public:
        Params(Params const &) = delete;

        void operator=(Params const &) = delete;
    };

}

#endif //STEMSALABIM_PARAMS_HPP
