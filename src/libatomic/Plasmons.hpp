/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_PLASMONS_HPP
#define STEMSALABIM_PLASMONS_HPP

#include <memory>
#include <mutex>
#include <algorithm>
#include <cmath>

#include <gsl/gsl_integration.h>

#include "../utilities/algorithms.hpp"
#include "../classes/GridManager.hpp"
#include "../classes/IO.hpp"

namespace stemsalabim { namespace atomic {

    class Plasmons {
    public:

        static Plasmons &getInstance() {
            static Plasmons instance;
            return instance;
        }

        void populateCache(const std::shared_ptr<GridManager> &gridman, double electron_density) {

            if(_cache_populated)
                return;

            Params &p = Params::getInstance();

            if(!p.plasmonsEnabled())
                return;

            unsigned int lx = gridman->samplingX();
            unsigned int ly = gridman->samplingY();
            double sdense2_x = pow(1. / 3. * gridman->densityX(), 2);
            double sdense2_y = pow(1. / 3. * gridman->densityY(), 2);

            gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000000);
            double error, result;
            gsl_function F;
            F.function = algorithms::unwrap;

            _cache.resize(gridman->energyLossGrid().size() - 1);

            //unsigned int energy_grid_index = 0;
            for(unsigned int eii = 0; eii < _cache.size(); ++eii) {

                _cache[eii].init(lx, ly);
                _cache[eii].setIsKSpace(true);

//                if(eii != 16)
//                    continue;

                for(unsigned int ix = 0; ix < lx; ix++) {
                    for(unsigned int iy = 0; iy < ly; iy++) {
                        double theta = sqrt(pow(gridman->kx(ix), 2) + pow(gridman->ky(iy), 2)) * p.wavelength();

                        if(!p.bandwidthLimiting() ||
                           pow(gridman->kx(ix), 2) / sdense2_x + pow(gridman->ky(iy), 2) / sdense2_y < 1) {
                            // integrate over energy

                            std::function<double(double)> func = [&](double x) {
                                return plasmonDoubleDifferentialCrossSection(x, theta, electron_density);
                            };

                            F.params = &func;

                            gsl_integration_qag(&F,
                                                gridman->energyLossGrid()[eii],
                                                gridman->energyLossGrid()[eii + 1],
                                                0,
                                                1e-7,
                                                1000000,
                                                GSL_INTEG_GAUSS61,
                                                w,
                                                &result,
                                                &error);
                            //result = func((gridman->energyLossGrid()[eii] + gridman->energyLossGrid()[eii+1])*0.5);
                            _cache[eii](ix, iy) = (float) sqrt(result);
                        }
                    }
                }

                _cache[eii].backwardFFT();
            }

            gsl_integration_workspace_free(w);

            _cache_populated = true;
        }

        const Wave &scatteringFunction(unsigned int energy_loss_bin_index) const {
            return _cache[energy_loss_bin_index];
        }

        double beta0() const {
            return _beta_0;
        }

        double beta1() const {
            return _beta_1;
        }

        double beta2() const {
            return _beta_2;
        }

    private:
        explicit Plasmons() {

            Params &p = Params::getInstance();

            _plasmon_energy = p.plasmonEnergy();
            _plasmon_fwhm = p.plasmonFWHM();
            _incident_energy = p.beamEnergy();
            _beta_0 = exp(-p.sliceThickness() / p.plasmonMeanFreePath());
            _beta_1 = p.sliceThickness() / p.plasmonMeanFreePath() * _beta_0;
            _beta_2 = p.sliceThickness() / p.plasmonMeanFreePath() * 0.5 * _beta_1;
        }

        /*!
         * Calculates the double differential cross section for plasmon scattering with an incident
         * electron wave by using the free electron gas model and the Lindhardt approximation.
         * @param E energy in eV
         * @param theta scattering angle in rad
         * @return double differential cross section in angstroms^2/eV
         */
        double
        plasmonDoubleDifferentialCrossSection(double E, double theta, double electron_density) {
            // calculate fermi energy in eV

            // this factor is pow(3*pi^2, 2./3.)
            constexpr double numerical_factor = 9.570780000627304;
            // this factor is hbar^2 / (2 * m_e * e) * 10^20 (eV nm^2)
            constexpr double numerical_factor_2 = 0.03809982080226884;
            constexpr double fermi_factor = numerical_factor_2 * numerical_factor;

            // fermi energy of free electron gas in eV
            double fermi_energy = pow(electron_density, 2. / 3.) * fermi_factor;

            // characteristic angle of plasmons in rad
            double characteristic_angle = (_incident_energy + ELECTRON_REST_MASS) /
                                          (2 * ELECTRON_REST_MASS + _incident_energy) *
                                          E /
                                          _incident_energy *
                                          1e-3;


            // plasmon dispersion
            double Ep = _plasmon_energy +
                        3. / 5. * fermi_energy / _plasmon_energy * 2 * _incident_energy * 1000 *
                        (theta * theta + characteristic_angle * characteristic_angle);

//            double g = 3. / 5. * fermi_energy / _plasmon_energy;
            // double differential cross section of plasmon scattering in angstroms^2/eV
            double B = sin(theta) / (theta * theta + characteristic_angle * characteristic_angle);
            B *= E * _plasmon_fwhm * Ep * Ep / (pow(E * E - Ep * Ep, 2.) + pow(E * _plasmon_fwhm, 2.));
            //B /= 1000;
//            double B = sin(theta) / (theta * theta + characteristic_angle * characteristic_angle);
//            B *= E * _plasmon_fwhm * _plasmon_energy * _plasmon_energy / (pow(E * E - _plasmon_energy * _plasmon_energy - 4*_plasmon_energy*_incident_energy*1000*g*(theta * theta + characteristic_angle * characteristic_angle), 2.) + pow(E * _plasmon_fwhm, 2.));
            return B;
        }


        double _incident_energy;
        double _plasmon_energy;
        double _plasmon_fwhm;
        double _beta_0;
        double _beta_1;
        double _beta_2;
        bool _cache_populated{false};

        std::vector<Wave> _cache;

        /// pi
        constexpr static double pi = 3.14159265358979323846;
        /// electron charge
        constexpr static double e = 1.60217662e-19;
        /// electron rest mass in keV. \f$m_0  c^2\f$ in keV
        constexpr static double ELECTRON_REST_MASS = 510.99906;
        /// bohr radius
        constexpr static double a0 = 5.2917721067e-11;
    };

}}
#endif //STEMSALABIM_PLASMONS_HPP
