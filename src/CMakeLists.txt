#
# STEMsalabim: Magical STEM image simulations
#
# Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
# Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


#SET(BUILD_SHARED_LIBS ON)

# compilation

set(INTERNAL_FILES
    libatomic/Scattering.hpp
    libatomic/Plasmons.hpp
    classes/Params.cpp           classes/Params.hpp
    classes/Simulation.cpp       classes/Simulation.hpp
    classes/IO.cpp               classes/IO.hpp
    classes/GridManager.cpp      classes/GridManager.hpp
    classes/FPConfManager.hpp
    classes/Slice.cpp            classes/Slice.hpp
    libatomic/Atom.hpp
    libatomic/Element.hpp
    libatomic/Cell.hpp
    libatomic/elements_json.hpp
    classes/SimulationState.hpp
    utilities/Wave.hpp
    utilities/mpi.hpp
    utilities/memory.hpp
    utilities/output.hpp
    utilities/initialize.hpp
    utilities/algorithms.hpp
    utilities/TaskQueue.hpp
    utilities/timings.hpp
    utilities/netcdf_utils.hpp
    utilities/ndarray.hpp)

set(EXTERNAL_FILES
    3rdparty/jsoncpp/jsoncpp.cpp)

set_source_files_properties(${INTERNAL_FILES}
                            PROPERTIES COMPILE_FLAGS "-Wall -Wextra -pedantic")

add_library(stemsalabim_lib ${INTERNAL_FILES} ${EXTERNAL_FILES})

add_executable(stemsalabim stemsalabim.cpp)
add_executable(ssb-chk ssb-chk.cpp)
add_executable(ssb-mkin ssb-mkin.cpp)
add_executable(ssb-run ssb-run.cpp)

include_directories(
        3rdparty/cereal/include
        3rdparty/args
        3rdparty/pybind11/include
        3rdparty/jsoncpp/json
        3rdparty/sole
        3rdparty/tinyformat
        3rdparty/variant-lite/include)


# linking ################################################

target_link_libraries(stemsalabim_lib m ${LIBS})
target_link_libraries(stemsalabim stemsalabim_lib)
target_link_libraries(ssb-chk stemsalabim_lib)
target_link_libraries(ssb-mkin stemsalabim_lib)
target_link_libraries(ssb-run stemsalabim_lib)

# MPI ####################################################

if(MPI_COMPILE_FLAGS)
    set_target_properties(stemsalabim PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif(MPI_COMPILE_FLAGS)

if(MPI_LINK_FLAGS)
    set_target_properties(stemsalabim PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}")
endif(MPI_LINK_FLAGS)

# installation ###########################################

install(TARGETS stemsalabim ssb-chk ssb-mkin ssb-run stemsalabim_lib
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)
