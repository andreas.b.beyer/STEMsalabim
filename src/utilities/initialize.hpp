/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

// This namespace contains only static methods to print out stuff to stdout.

#ifndef INITIALIZE_HPP_
#define INITIALIZE_HPP_

#include <thread>
#include "mpi.hpp"
#include "config.h"
#include "TaskQueue.hpp"
#include "../classes/Params.hpp"
#include "../classes/IO.hpp"
#include "../classes/FPConfManager.hpp"
#include "../classes/SimulationState.hpp"

namespace stemsalabim {

    using namespace std;
    namespace op = stemsalabim::output;

    inline void initialize_input_file(int argc, const char **argv, bool silent = false) {
        auto &mpi_env = mpi::Environment::getInstance();
        Params &p = Params::getInstance();

        p.initFromCLI(argc, argv);

        if(mpi_env.isMaster()) {
            p.readParamsFromString(algorithms::readTextFile(p.paramsFileName()));
            p.setCell(IO::initCrystalFromXYZFile(p.crystalFilename()));

            auto fpman = std::make_shared<FPConfManager>(p.cell());
            auto gridman = std::make_shared<GridManager>(p.cell());
            auto io = std::make_shared<IO>();

            fpman->generateDisplacements();
            gridman->generateGrids();

            io->initNCFile(gridman, p.cell());
            io->writeParams(gridman);
            io->writeGrids(gridman);

            p.cell()->initScattering();

            SimulationStateManager state_manager(gridman, false);

            // loop over simulations
            for(SimulationState &st: state_manager) {

                fpman->setConfiguration(st.iteration());

                // in each configuration, assign atoms to the corresponding slices.
                fpman->assignAtomsToSlices(gridman);

                io->writeCrystal(fpman, p.cell(), gridman);

                if(p.storedPotentials()) {
                    std::vector<std::vector<std::vector<float>>> vz(gridman->slices().size());

                    if(!silent)
                        output::print(output::fmt("Calculating potentials for defocus %d, configuration %d...\n"),
                                  st.idefocus(),
                                  st.iconf());

                    TaskQueue slice_work;
                    slice_work.append(gridman->slices());

                    std::vector<std::thread> threads;
                    for(unsigned int nt = 0; nt < p.numberOfThreads(); nt++) {
                        threads.emplace_back([&] {
                            unsigned int slic_index;
                            while(slice_work.pop(slic_index)) {
                                vz[slic_index] = gridman->slices()[slic_index]->calculatePotential(fpman, gridman);
                                slice_work.finish(slic_index);
                            }
                        });
                    }

                    slice_work.waitUntilFinished();
                    for(auto &t : threads)
                        t.join();

                    io->writePotentials(fpman, vz);
                }
            }
        }
    }

    inline void printStatus(const std::shared_ptr<GridManager> & gridman) {
        Params &p = Params::getInstance();

        double kx_max = 1.0 / (3.0 * max(1. / gridman->scaleX(), 1. / gridman->scaleY())) * 1000;

        size_t mem_per_proc = 0, result_mem_per_proc = 0;
        size_t fs = sizeof(float);

        // transmission functions
        mem_per_proc += gridman->samplingX() * gridman->samplingY() * 2 * fs * gridman->slices().size();

        // wave functions for each thread during pixel calculation
        mem_per_proc += gridman->samplingX() *
                        gridman->samplingY() *
                        2 *
                        fs *
                        (gridman->energyLossGrid().size() + 2) *
                        p.numberOfThreads();


        // results
        unsigned long number_intensities_per_pixel = p.adf() ? (gridman->adfDetectorGrid().size() *
                                                                gridman->adfSliceCoords().size() *
                                                                gridman->energyLossGrid().size()) : 0;
        unsigned long number_cbed_per_pixel = p.cbed() ? (gridman->storedCbedSizeX() *
                                                          gridman->storedCbedSizeY() *
                                                          gridman->cbedSliceCoords().size() *
                                                          gridman->energyLossGrid().size()) : 0;

        // buffer arrays in calculatePixel
        result_mem_per_proc += p.numberOfThreads() * (number_intensities_per_pixel + number_cbed_per_pixel) * fs;

        // buffers at least!
        result_mem_per_proc += p.workPackageSize() * (number_intensities_per_pixel + number_cbed_per_pixel) * fs;

        // output file size
        // adf
        unsigned long num_adf = gridman->adfDetectorGrid().size() *
                                gridman->adfSliceCoords().size() *
                                gridman->adfXGrid().size() *
                                gridman->adfYGrid().size() *
                                gridman->energyLossGrid().size() *
                                fs;
        if(!p.adfAverageConfigurations())
            num_adf *= p.numberOfConfigurations();

        if(!p.adfAverageDefoci())
            num_adf *= p.numberOfDefoci();

        // cbed
        unsigned long num_cbed = gridman->storedCbedSizeX() *
                                 gridman->storedCbedSizeY() *
                                 gridman->cbedSliceCoords().size() *
                                 gridman->cbedXGrid().size() *
                                 gridman->cbedYGrid().size() *
                                 gridman->energyLossGrid().size() *
                                 fs;
        if(!p.cbedAverageConfigurations())
            num_cbed *= p.numberOfConfigurations();

        if(!p.cbedAverageDefoci())
            num_cbed *= p.numberOfDefoci();

        // com
        // cbed
        unsigned long num_com = 2 *
                                gridman->adfSliceCoords().size() *
                                gridman->adfXGrid().size() *
                                gridman->adfYGrid().size() *
                                gridman->energyLossGrid().size() *
                                fs;

        if(!p.adfAverageConfigurations())
            num_com *= p.numberOfConfigurations();

        if(!p.adfAverageDefoci())
            num_com *= p.numberOfDefoci();

        if(!p.cbed())
            num_cbed = 0;
        if(!p.adf()) {
            num_com = 0;
            num_adf = 0;
        }

        // elements
        string el_str;
        unsigned int counter;
        for(auto & e: p.cell()->elements()) {
            el_str += " " + e->symbol();

            counter = 0;
            for(auto & a: p.cell()->getAtoms()) {
                if(a->getElement()->symbol() == e->symbol()) {
                    counter++;
                }
            }
            el_str += output::fmt(" (%d)", counter);
        }

        float kb_to_GB = powf(1024.f, 3);

        op::nakedprint("\n\n");
        op::nakedprint("%s v%s.%s.%s\n", PKG_NAME, PKG_VERSION_MAJOR, PKG_VERSION_MINOR, PKG_VERSION_PATCH);
        op::nakedprint("   git commit %s\n", GIT_COMMIT_HASH);
        op::nakedprint("   build type %s\n", BUILD_TYPE);
        op::nakedprint("   build date %s\n", BUILD_DATE);

        op::nakedprint("\nGeneral information\n");
        op::nakedprint("-----------------------------------\n");
        op::nakedprint("   Grid size:        %d x %d\n", gridman->samplingX(), gridman->samplingY());
        op::nakedprint("   Scan points:      %d x %d\n", gridman->adfXGrid().size(), gridman->adfYGrid().size());
        op::nakedprint("   Energy bins:      %d\n", gridman->energyLossGrid().size());
        op::nakedprint("   Wave length       %.4f nm\n", p.wavelength());
        op::nakedprint("   Max angle         %.1f mrad\n", kx_max * p.wavelength());
        op::nakedprint("   Angle resolution: %.3f mrad\n", gridman->kx(1) * p.wavelength() * 1000);
        op::nakedprint("   Num FP configs:   %d\n", p.numberOfConfigurations());
        op::nakedprint("   Num Defoci:       %d\n", p.numberOfDefoci());

        op::nakedprint("\nCrystal\n");
        op::nakedprint("-----------------------------------\n");
        op::nakedprint("   Number of atoms:  %d\n", p.cell()->numberOfAtoms());
        op::nakedprint("   Number of slices: %d\n", gridman->numberOfSlices());
        op::nakedprint("   Cell size:        %.2f x %.2f x %.2f nm\n", p.cell()->sizeX(), p.cell()->sizeY(), p.cell()->sizeZ());
        op::nakedprint("   Elements         %s\n", el_str);

        op::nakedprint("\nMemory estimates (lower bounds!!)\n");
        op::nakedprint("-----------------------------------\n");
        op::nakedprint("   Working mem/proc: %.1f GB\n", mem_per_proc / kb_to_GB);
        op::nakedprint("   Results mem/proc: %.1f GB\n", result_mem_per_proc / kb_to_GB);
        op::nakedprint("   Total mem/proc:   %.1f GB\n",
                       (result_mem_per_proc + mem_per_proc) / kb_to_GB);

        op::nakedprint("\nOutput file size\n");
        op::nakedprint("-----------------------------------\n");
        op::nakedprint("   ADF storage:      %.1f GB\n", num_adf / kb_to_GB);
        op::nakedprint("   Center of mass:   %.1f GB\n", num_com / kb_to_GB);
        op::nakedprint("   CBED storage:     %.1f GB\n", num_cbed / kb_to_GB);
        op::nakedprint("   Total size:       %.1f GB\n", (num_cbed + num_adf + num_com) / kb_to_GB);

        op::nakedprint("\n\n");

    }

}
#endif // INITIALIZE_HPP_