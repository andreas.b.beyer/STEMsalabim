# This module defines
# NETCDF_INCLUDE_DIR, where to find include files, etc.
# NETCDF_LIBRARIES, the libraries to link against
# NETCDF_STATIC_LIBRARIY_PATH
# NETCDF_FOUND if it was found
#
# The following variables will be checked by the function
#   NETCDF_ROOT      ... if set, the libraries are exclusively searched under this path

MESSAGE(STATUS "Searching for NetCDF libraries")


# Check if we can use PkgConfig
FIND_PACKAGE(PkgConfig)

#Determine from PKG
IF( PKG_CONFIG_FOUND AND NOT NETCDF_ROOT )
    pkg_check_modules( PKG_NETCDF QUIET "netcdf" )
ENDIF()

IF( NETCDF_ROOT )

    FIND_LIBRARY(
            NETCDF_LIBRARIES
            NAMES "netcdf"
            PATHS ${NETCDF_ROOT}
            PATH_SUFFIXES "lib" "lib64"
            NO_DEFAULT_PATH
    )

    FIND_PATH(
            NETCDF_INCLUDE_DIR
            NAMES "netcdf.h"
            PATHS ${NETCDF_ROOT}
            PATH_SUFFIXES "include"
            NO_DEFAULT_PATH
    )

ELSE( NETCDF_ROOT )

    FIND_LIBRARY(
            NETCDF_LIBRARIES
            NAMES "netcdf"
            PATHS ${PKG_NETCDF_LIBRARY_DIRS} ${LIB_INSTALL_DIR}
    )

    FIND_PATH(
            NETCDF_INCLUDE_DIR
            NAMES "netcdf.h"
            PATHS ${PKG_NETCDF_INCLUDE_DIRS} ${INCLUDE_INSTALL_DIR}
    )

ENDIF( NETCDF_ROOT )

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(NETCDF DEFAULT_MSG NETCDF_INCLUDE_DIR NETCDF_LIBRARIES)

MARK_AS_ADVANCED(
        NETCDF_INCLUDE_DIR
        NETCDF_LIBRARIES
)
