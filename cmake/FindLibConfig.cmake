# This module defines
# LIBCONFIG_INCLUDE_DIR, where to find include files, etc.
# LIBCONFIG_LIBRARIES, the libraries to link against
# LIBCONFIG_STATIC_LIBRARIY_PATH
# LIBCONFIG_FOUND, If false, do not try to use CppUnit.
#
# The following variables will be checked by the function
#   LIBCONFIG_ROOT      ... if set, the libraries are exclusively searched under this path
# also defined, but not for general use are
# LIBCONFIG_LIBRARY, where to find the CUnit library.

MESSAGE(STATUS "Searching for libconfig library")

# Check if we can use PkgConfig
FIND_PACKAGE(PkgConfig)

#Determine from PKG
IF( PKG_CONFIG_FOUND AND NOT LIBCONFIG_ROOT )
    pkg_check_modules( PKG_LIBCONFIG QUIET "libconfig" )
ENDIF()

IF( LIBCONFIG_ROOT )

    FIND_LIBRARY(
            LIBCONFIG_LIBRARIES
            NAMES "config++" "libconfig++"
            PATHS ${LIBCONFIG_ROOT}
            PATH_SUFFIXES "lib" "lib64"
            NO_DEFAULT_PATH
    )

    FIND_PATH(
            LIBCONFIG_INCLUDE_DIR
            NAMES "libconfig.h++"
            PATHS ${LIBCONFIG_ROOT}
            PATH_SUFFIXES "include"
            NO_DEFAULT_PATH
    )

ELSE( LIBCONFIG_ROOT )

    FIND_LIBRARY(
            LIBCONFIG_LIBRARIES
            NAMES "config++"  "libconfig++"
            PATHS ${PKG_LIBCONFIG_LIBRARY_DIRS} ${INCLUDE_INSTALL_DIR}
    )

    FIND_PATH(
            LIBCONFIG_INCLUDE_DIR
            NAMES "libconfig.h++"
            PATHS ${PKG_LIBCONFIG_INCLUDE_DIRS} ${LIB_INSTALL_DIR}
    )

ENDIF( LIBCONFIG_ROOT )

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LIBCONFIG DEFAULT_MSG LIBCONFIG_INCLUDE_DIR LIBCONFIG_LIBRARIES)
MARK_AS_ADVANCED(
        LIBCONFIG_INCLUDE_DIR
        LIBCONFIG_LIBRARIES
)